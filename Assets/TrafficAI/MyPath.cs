﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPath : MonoBehaviour
{
    public Color lineColor;

    private GameObject currentNode;

    private void OnDrawGizmos()
    {
        Gizmos.color = lineColor;
        currentNode = transform.GetChild(0).gameObject;
        tree(currentNode);
    }

    private void tree(GameObject gameObject)
    {
        MyNode myNode = gameObject.GetComponent<MyNode>();
        Gizmos.DrawWireSphere(gameObject.transform.position, 0.2f);
        if (gameObject.GetInstanceID() != myNode.leftNode.GetInstanceID())
        {
            Gizmos.DrawLine(gameObject.transform.position, myNode.leftNode.transform.position);
            tree(myNode.leftNode);
        }
        if (gameObject.GetInstanceID() != myNode.centerNode.GetInstanceID())
        {
            Gizmos.DrawLine(gameObject.transform.position, myNode.centerNode.transform.position);
            tree(myNode.centerNode);
        }
        if (gameObject.GetInstanceID() != myNode.rightNode.GetInstanceID())
        {
            Gizmos.DrawLine(gameObject.transform.position, myNode.rightNode.transform.position);
            tree(myNode.rightNode);
        }
    }
}
