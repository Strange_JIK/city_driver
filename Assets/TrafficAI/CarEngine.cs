﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarEngine : MonoBehaviour
{
    public Transform startNode;
    public float maxSteerAngle = 45f;
    public float turnSpeed = 5f;
    public WheelCollider wheelFL;
    public WheelCollider wheelFR;
    public WheelCollider wheelRL;
    public WheelCollider wheelRR;
    public float maxMotorTorque = 80f;
    public float maxBrakeTorque = 150f;
    public float currentSpeed;
    public float maxSpeed = 100f;
    public Vector3 centerOfMass;
    public bool isBraking = false;

    [Header("Sensor")]
    public float sensorLength = 3f;
    public Vector3 frontSensorPosition = new Vector3(0f, 0.2f, 0.5f);
    public float frontSideSensorPosition = 0.3f;
    public float frontSensorAngle = 30f;

    private Transform currentNode;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody>().centerOfMass = centerOfMass;
        currentNode = startNode;
    }

    // Update is called once per frame
    void Update()
    {
        Sensor();
        ApplySteer();
        Drive();
        CheckWayPointDistance();
        Braking();
    }

    private void Sensor()
    {
        RaycastHit hit;
        Vector3 sensorStartPos = transform.position;
        sensorStartPos += transform.forward * frontSensorPosition.z;
        sensorStartPos += transform.up * frontSensorPosition.y;

        // front right sensor
        sensorStartPos += transform.right * frontSideSensorPosition;
        if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength))
        {
            if (!hit.collider.CompareTag("Terrain"))
            {
                Debug.DrawLine(sensorStartPos, hit.point);
                isBraking = true;
            }
        }

        // front right angle sensor
        else if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength))
        {
            if (!hit.collider.CompareTag("Terrain"))
            {
                Debug.DrawLine(sensorStartPos, hit.point);
                isBraking = true;
            }
        }

        // front left sensor
        sensorStartPos -= 2 * transform.right * frontSideSensorPosition;
        if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength))
        {
            if (!hit.collider.CompareTag("Terrain"))
            {
                Debug.DrawLine(sensorStartPos, hit.point);
                isBraking = true;
            }
        }

        // front left angle sensor
        else if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(-frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength))
        {
            if (!hit.collider.CompareTag("Terrain"))
            {
                Debug.DrawLine(sensorStartPos, hit.point);
                isBraking = true;
            }
        }

        // front center sensor
        sensorStartPos += transform.right * frontSideSensorPosition;
        if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength))
        {
            if (!hit.collider.CompareTag("Terrain"))
            {
                Debug.DrawLine(sensorStartPos, hit.point);
                isBraking = true;
            }
        }
    }

    private void ApplySteer()
    {
        Vector3 relativeVector = transform.InverseTransformPoint(currentNode.position);
        float newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;
        wheelFL.steerAngle = newSteer;
        wheelFR.steerAngle = newSteer;
    }

    private void Drive()
    {
        currentSpeed = 2 * Mathf.PI * wheelFL.radius * wheelFL.rpm * 60 / 1000;

        if (currentSpeed < maxSpeed && !isBraking)
        {
            wheelFL.motorTorque = maxMotorTorque;
            wheelFR.motorTorque = maxMotorTorque;
        }
        else
        {
            wheelFL.motorTorque = 0;
            wheelFR.motorTorque = 0;
        }
    }

    private void CheckWayPointDistance()
    {
        if (Vector3.Distance(transform.position, currentNode.position) < 1)
        {
            MyNode myNode = currentNode.GetComponent<MyNode>();
            int route = 0;
            bool leftN = false;
            bool centerN = false;
            bool rightN = false;
            if (myNode.leftNode.GetInstanceID() != currentNode.GetInstanceID())
            {
                ++route;
                leftN = true;
            }
            if (myNode.centerNode.GetInstanceID() != currentNode.GetInstanceID())
            {
                ++route;
                centerN = true;
            }
            if (myNode.rightNode.GetInstanceID() != currentNode.GetInstanceID())
            {
                ++route;
                rightN = true;
            }
            switch (route)
            {
                case 3:
                    {
                        int selectNode = Random.Range(0, 3);
                        switch (selectNode)
                        {
                            case 0:
                                currentNode = myNode.leftNode.transform;
                                break;
                            case 1:
                                currentNode = myNode.centerNode.transform;
                                break;
                            case 2:
                                currentNode = myNode.rightNode.transform;
                                break;
                        }
                        break;
                    }
                case 2:
                    {
                        if (leftN == false)
                        {
                            int selectNode = Random.Range(0, 2);
                            switch (selectNode)
                            {
                                case 0:
                                    currentNode = myNode.centerNode.transform;
                                    break;
                                case 1:
                                    currentNode = myNode.rightNode.transform;
                                    break;
                            }
                        }
                        else if (centerN == false)
                        {
                            int selectNode = Random.Range(0, 2);
                            switch (selectNode)
                            {
                                case 0:
                                    currentNode = myNode.leftNode.transform;
                                    break;
                                case 1:
                                    currentNode = myNode.rightNode.transform;
                                    break;
                            }
                        }
                        else if (rightN == false)
                        {
                            int selectNode = Random.Range(0, 2);
                            switch (selectNode)
                            {
                                case 0:
                                    currentNode = myNode.leftNode.transform;
                                    break;
                                case 1:
                                    currentNode = myNode.centerNode.transform;
                                    break;
                            }
                        }
                        break;
                    }
                case 1:
                    {
                        if (leftN == true)
                        {
                            currentNode = myNode.leftNode.transform;
                        }
                        else if (centerN == true)
                        {
                            currentNode = myNode.centerNode.transform;
                        }
                        else if (rightN == true)
                        {
                            currentNode = myNode.rightNode.transform;
                        }
                        break;
                    }
            }
        }
    }

    private void Braking()
    {
        if (isBraking)
        {
            wheelRL.brakeTorque = maxBrakeTorque;
            wheelRR.brakeTorque = maxBrakeTorque;
        }
        else
        {
            wheelRL.brakeTorque = 0;
            wheelRR.brakeTorque = 0;
        }
    }
}
