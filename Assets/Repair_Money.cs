﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Repair_Money : MonoBehaviour
{

    public int money;
    void Start()
    {
     //구매 여부 확인   
    }

    // Update is called once per frame
   public void Update()
    {
        money = GameObject.Find("GameMoney").GetComponent<GameMoney>().Money;
        if (Input.GetKeyDown("0"))
        {
            GameObject.Find("GameMoney").GetComponent<GameMoney>().CountUp(100000);
        }
    }
    public void Buy_O()
    {
        if (money >= 1000)
        {
            GameObject.Find("O_Lock").GetComponent<Button>().interactable = true;
            GameObject.Find("GameMoney").GetComponent<GameMoney>().CountDown(1000);
            GameObject.Find("O_Lock (1)").gameObject.SetActive(false);
        }
        
    }
    public void Buy_P()
    {
        if (money >= 1000)
        {
            GameObject.Find("P_Lock").GetComponent<Button>().interactable = true;
            GameObject.Find("GameMoney").GetComponent<GameMoney>().CountDown(1000);
            GameObject.Find("P_Lock (1)").gameObject.SetActive(false);
        }
    }
    public void Buy_B()
    {
        if (money >= 1500)
        {
            GameObject.Find("B_Lock").GetComponent<Button>().interactable = true;
            GameObject.Find("GameMoney").GetComponent<GameMoney>().CountDown(1500);
            GameObject.Find("B_Lock (1)").gameObject.SetActive(false);
        }
    }
    public void Buy_W()
    {
        if (money >= 1500)
        {
            GameObject.Find("W_Lock").GetComponent<Button>().interactable = true;
            GameObject.Find("GameMoney").GetComponent<GameMoney>().CountDown(1500);
            GameObject.Find("W_Lock (1)").gameObject.SetActive(false);
        }
    }
    public void Buy_Y()
    {
        if (money >= 2000)
        {
            GameObject.Find("Y_Lock").GetComponent<Button>().interactable = true;
            GameObject.Find("GameMoney").GetComponent<GameMoney>().CountDown(2000);
            GameObject.Find("Y_Lock (1)").gameObject.SetActive(false);
        }
    }
    public void Buy_Blue()
    {
        if (money >= 2000)
        {
            GameObject.Find("BL_Lock").GetComponent<Button>().interactable = true;
            GameObject.Find("GameMoney").GetComponent<GameMoney>().CountDown(2000);
            GameObject.Find("BL_Lock (1)").gameObject.SetActive(false);
        }
    }
    public void Buy_Wheel()
    {
        if (money >= 5000)
        {
            GameObject.Find("WH_Lock").GetComponent<Button>().interactable = true;
            GameObject.Find("GameMoney").GetComponent<GameMoney>().CountDown(5000);
            GameObject.Find("WH_Lock (1)").gameObject.SetActive(false);
        }
    }
}
