﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameMoney : MonoBehaviour
{
    public int Money;
    public Text Money_text;
    void Start()
    {
        Money = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void CountUp(int price)
    {
        StartCoroutine(Count(Money + price, Money));
    }
    public void CountDown(int price)
    {
        StartCoroutine(Count_D(Money - price, Money));
    } 
    IEnumerator Count(float target, float current)

    {
        float duration = 1; // 카운팅에 걸리는 시간 설정. 
        float offset = (target - current) / duration;

        while (current < target)
        {
            current += offset * Time.deltaTime;

            Money_text.text = "$" + ((int)current).ToString("D9");
            //current = target;
            yield return null;
        }
        current = target;
        Money = (int)current;
        Money_text.text = "$" + ((int)current).ToString("D9");

    }
    IEnumerator Count_D(float target, float current)
                         /// 100               150
    {
        float duration = 1; // 카운팅에 걸리는 시간 설정. 
        float offset = (current - target) / duration;

        while (current > target)
        {
            current -= offset * Time.deltaTime;

            Money_text.text = "$" + ((int)current).ToString("D9");
          //  current = target;
            yield return null;
        }
        current = target;
        Money = (int)current;
        Money_text.text = "$" + ((int)current).ToString("D9");

    }
}
