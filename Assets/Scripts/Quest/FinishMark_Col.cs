﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class FinishMark_Col : MonoBehaviour
{
    GameObject[] FinishMark;
    public UnityEvent onEventStart;
    // Start is called before the first frame update
    void Start()
    {
      
    }
    private void Update()
    {
        if (GameObject.FindGameObjectsWithTag("Item_Finish") != null)
        {

            FinishMark = GameObject.FindGameObjectsWithTag("Item_Finish");
        }
    }
    void OnTriggerEnter(Collider other)
    {
        for (int i = 0; i < FinishMark.Length; i++)
        {
            if (other.gameObject == FinishMark[i])
            {
                FinishMark[i].gameObject.SetActive(false);
               
                onEventStart.Invoke();
            }
        }

    }
}
