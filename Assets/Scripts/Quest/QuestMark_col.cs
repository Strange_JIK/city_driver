﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Vehicles.Car;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;



public class QuestMark_col : MonoBehaviour
{
    GameObject[] QuestMark;
    GameObject[] FinishMark;
    GameObject[] Booster;
    GameObject[] RepairShop;
    public GameObject Dest;
    public int num;
    public int quest_save;
    public GameObject ES;
    public bool Fin;
    private Vector3[] Dest_pos;
    public int[] Timer;


    public UnityEvent onEventStart, onEventEnd, onBoosterEvent, onRepairEvent;

    AudioSource[] AS;
    // Start is called before the first frame update
    void Start()
    { 
        Dest_pos = new Vector3[40];
        Timer = new int[40];
        // GameObject.Find("GameMoney").GetComponent<GameMoney>().Money;
    }
    public void Update()
    {
        
        if (GameObject.FindGameObjectsWithTag("Item") != null)
        {

            QuestMark = GameObject.FindGameObjectsWithTag("Item");
            for (int i = 0; i < QuestMark.Length; i++)
            {
            
                   Dest_pos[i] =QuestMark[i].GetComponent<Quest_num>().Destiny_pos;
                   Timer[i] = QuestMark[i].GetComponent<Quest_num>().TImer;
            }
        }
        if (GameObject.FindGameObjectsWithTag("Item_Finish") != null)
        {

            FinishMark = GameObject.FindGameObjectsWithTag("Item_Finish");
        }
        if (GameObject.FindGameObjectsWithTag("N2O") != null)
        {

            Booster = GameObject.FindGameObjectsWithTag("N2O");
        }
        if (GameObject.FindGameObjectsWithTag("RepairShop") != null)
        {

            RepairShop = GameObject.FindGameObjectsWithTag("RepairShop");
        }
       
    }
    void OnTriggerEnter(Collider other)
    {
        for (int i = 0; i < QuestMark.Length; i++)
        {
            if (other.gameObject == QuestMark[i])
            {
                QuestMark[i].gameObject.SetActive(false);
                quest_save = i;
                Instantiate(Dest, Dest_pos[i], Quaternion.identity);
                onEventStart.Invoke();
                //Debug.Log(i);
            }
        }
        for (int i = 0; i < FinishMark.Length; i++)
        {
            if (other.gameObject == FinishMark[i])
            {
                FinishMark[i].gameObject.SetActive(false);
                GameObject.Find("GameMoney").GetComponent<GameMoney>().CountUp(30000);
                onEventEnd.Invoke();
                //Debug.Log(i);
               // Debug.Log("END");
            }
        }
        for (int i = 0; i < Booster.Length; i++)
        {
            if (other.gameObject == Booster[i])
            {
                Booster[i].gameObject.SetActive(false);
                GameObject.Find("GameMoney").GetComponent<GameMoney>().CountUp(1500);


            }
        }
        for (int i = 0; i < RepairShop.Length; i++)
        {
            if (other.gameObject == RepairShop[i])
            {
                GameObject.Find("streamer_master").GetComponent<streamer_M>().DestoryAll();

                onRepairEvent.Invoke();
                this.GetComponent<VehicleControl>().repair_on = true;
                GameObject.Find("CameraGroup").transform.GetChild(0).gameObject.SetActive(false);
                GameObject.FindGameObjectWithTag("MainUI").transform.GetChild(0).gameObject.SetActive(false);
                GameObject.Find("RepairShop_Main").transform.GetChild(0).GetChild(0).gameObject.SetActive(true); //cam
                GameObject.Find("RepairShop_Main").transform.GetChild(4).gameObject.SetActive(true); //UI


              
          
                //AS = this.GetComponents<AudioSource>();
                //for(int j=0;j<AS.Length;j++)
                //{
                //    AS[j].volume = 0;
                //}
                GameObject.Find("BGM").GetComponent<AudioSource>().enabled = false;
            }
        }
    }
    



   
}
