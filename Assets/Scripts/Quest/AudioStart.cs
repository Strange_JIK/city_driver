﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioStart : MonoBehaviour
{
    private int count;
    private bool count_B;
    GameObject ES;
    GameObject QM;
    GameObject[] FinishMark;
    GameObject[] QuestMark = new GameObject[40];
    public void OnEnable()
    {
        count = 0;
        count_B = false;
        ES = GameObject.Find("EventSystem").transform.GetChild(0).gameObject;
    }
    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("QuestionGroup") != null)
        {

            QM = GameObject.Find("QuestionGroup");
            Debug.Log(QM.transform.childCount);
            for (int i = 0; i < QM.transform.childCount; i++)
            {
                QuestMark[i] = QM.transform.GetChild(i).gameObject;
            }
        }
        if (GameObject.FindGameObjectsWithTag("Item_Finish") != null)
        {
            FinishMark = GameObject.FindGameObjectsWithTag("Item_Finish");
        }
        count_B = ES.GetComponent<Event_Start>().count_b;
        if (count_B)
        {
            count += 1;
        }
        if (count > 60)
        {
            for (int i = 0; i < FinishMark.Length; i++)
            {

                FinishMark[i].gameObject.SetActive(false);
               
            }
            Debug.Log(QuestMark.Length);
            for (int i = 0; i < QM.transform.childCount; i++)
            {

                QuestMark[i].gameObject.SetActive(true);
            }
            GameObject.Find("EventUI").transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
            GameObject.Find("EventUI").transform.GetChild(0).GetChild(2).gameObject.SetActive(false);
           
        }
    }
}
