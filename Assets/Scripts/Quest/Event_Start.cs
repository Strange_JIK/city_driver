﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;

public class Event_Start : MonoBehaviour
{
    
    public float Timer = 10f;
    private float Timer_dot;
    public Text[] objtext;
    public GameObject Event_UI;
    public GameObject Logo;
    float TIme_Save = 0;
    public bool Finish_on;
    public bool count_b;
    public UnityEvent onEndStart;

    public int[] QC_Timer;
    public int QC_count;

    public void OnEnable()
    {
          QC_Timer = GameObject.FindGameObjectWithTag("Player").GetComponent<QuestMark_col>().Timer;
         QC_count = GameObject.FindGameObjectWithTag("Player").GetComponent<QuestMark_col>().quest_save;
        Timer= (float)QC_Timer[QC_count];
         count_b = false;
            TIme_Save = Timer;
            Event_UI.SetActive(true);
            //StartCoroutine(a());
            //Debug.Log("AWAKE");
    }
    private void Update()
    {
            if (Timer <= 0)
            {
            onEndStart.Invoke();
            //Timer = TIme_Save
            //game over
        }
            if (Timer > 0)
            {

                Timer -= Time.deltaTime;
                objtext[0].text = Mathf.Ceil(Timer).ToString();
                Timer_dot = Timer - Mathf.Ceil(Timer);
                objtext[1].text = Mathf.Ceil(Timer_dot * 100).ToString();
                //objtext.text = "" + Timer;
            }
          
    }

    public void Finish()
    {
        GameObject.Find("EventUI").transform.GetChild(0).GetChild(1).gameObject.SetActive(true);
        this.gameObject.SetActive(false);
        Event_UI.SetActive(false);
        count_b = true;
        Timer = TIme_Save;
    }
    public void Finish_2()
    {
        GameObject.Find("EventUI").transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
        this.gameObject.SetActive(false);
        Event_UI.SetActive(false);
        count_b = true;
        Timer = TIme_Save;
    }
    
}
