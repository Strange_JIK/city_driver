﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class Logo_event : MonoBehaviour
{
    public UnityEvent onEventStart;
    
    // Update is called once per frame
    void Update()
    {
        if (!this.GetComponent<Animation>().isActiveAndEnabled)
        {
            onEventStart.Invoke();

        }
    }
    public void play()
    {
        this.GetComponent<Animation>().Play("logo_Move");
    }
    public void rewind()
    {
    }
}
