﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Dont_destroy_on_load : MonoBehaviour
{
    void Awake()
    {
        DontDestroyOnLoad(this);
    }
    void Update()
    {
        if (SceneManager.GetActiveScene().name == "03.MainMap")
        {
            Debug.Log("!!!!!!!!!!!!");
            GameObject.Destroy(this.gameObject);
        }
    }
}