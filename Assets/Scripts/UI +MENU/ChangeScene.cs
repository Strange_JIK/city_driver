﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public GameObject Car_Number;
    public bool scene_check = false;
    private void Start()
    {
        if (!scene_check)
        {
            Car_Number = GameObject.Find("Car_Number");
        }
    }
    public void ChangeFirstScene()
    {
        SceneManager.LoadScene("01.MainMenu");
    
    }
    public void ChangeSecondScene()
    {
        SceneManager.LoadScene("02.CarSelect");

    }
    public void ChangeThirdScene()
    {

        SceneManager.LoadScene("03.MainMap");
        if (!scene_check)
        {
            DontDestroyOnLoad(Car_Number);
        }
    }
}
