﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestArrow : MonoBehaviour
{
    public Transform target;
    public Transform Start;
    void Update()
    {
        Start = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        target = GameObject.FindGameObjectWithTag("Item_Finish").GetComponent<Transform>();
        Vector3 vec = target.position - Start.transform.position;
        vec.Normalize();
        Quaternion q = Quaternion.LookRotation(vec);
        this.transform.rotation =q;
    }
}
