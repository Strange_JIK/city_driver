﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    private Transform move2d;
    public float speed =0.1f;
    private bool on = false;
    private Vector3 start = new Vector3(399.49f ,327.05f, 425.77f);
    private Vector3 end = new Vector3(385.66f, 327.02f, 425.95f);
    //(-1.15f,0.86f,0.8f);
    // Start is called before the first frame update
    void Start()
    {
        move2d = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {

        if (on) {
            move2d.position = Vector3.MoveTowards(move2d.position, start, speed * Time.deltaTime);
        }
        else {
            move2d.position = Vector3.MoveTowards(move2d.position, end, speed * Time.deltaTime);
        }
        if (move2d.position.x == end.x) { on = true; }
        else if (move2d.position.x == start.x) { on = false; }

    }
}
