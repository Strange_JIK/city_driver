﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Palate : MonoBehaviour
{
    public Material MATERIAL;
    public MeshRenderer[] PAINT;
    public void OnEnable()
    {
        PAINT = GameObject.FindGameObjectWithTag("painter").GetComponents<MeshRenderer>();
        MATERIAL = PAINT[0].sharedMaterial;
    }
    public void Red()
    {
        MATERIAL.color = Color.red;
    }
    public void Blue()
    {
        MATERIAL.color = Color.blue;
    }
    public void Green()
    {
        MATERIAL.color = Color.green;
    }
    public void White()
    {
        MATERIAL.color = Color.white;
    }
    public void Black()
    {
        MATERIAL.color = Color.black;
    }
    public void Yellow()
    {
        MATERIAL.color = Color.yellow;
    }
    public void Orange()
    {
        MATERIAL.color = new Color(255/255f, 130 / 255f, 0 / 255f);
    }
    public void Purple()
    {
        MATERIAL.color = Color.magenta;
    }
}
