﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Vehicles.Car;
using UnityEngine;

public class CamMove : MonoBehaviour
{
    public Camera cam;
    
    struct MovePoint
    {
        public Vector3 Pos;
        public Vector3 Rotation;

    };
    MovePoint[] MP = new MovePoint[3];
    bool Palate_B = false, Wheel_B = false, Back_B = false;
    private void Start()
    {

        MP[0].Pos = new Vector3(-3.09f,1.09f,-1.55f);
        MP[0].Rotation = new Vector3(17.1f, -84.29f, 0);
            

        MP[1].Pos = new Vector3(-4.438f, 2.52f, -1.41f);
        MP[1].Rotation = new Vector3(43.43f, -84.29f, 0);


        MP[2].Pos = new Vector3(-6.46f,0.34f,0.02f);
        MP[2].Rotation = new Vector3(26.45f, -168f, 0);
    }
    private void Update()
    {
        if(Palate_B)
        {
            cam.transform.localPosition = Vector3.Lerp(cam.transform.localPosition, MP[1].Pos, Time.deltaTime * 0.8f);
            cam.transform.localRotation = Quaternion.Lerp(cam.transform.localRotation, Quaternion.Euler(MP[1].Rotation), Time.deltaTime * 0.8f);

        }
        if(Wheel_B)
        {
            cam.transform.localPosition = Vector3.Lerp(cam.transform.localPosition, MP[2].Pos, Time.deltaTime * 0.8f);
            cam.transform.localRotation = Quaternion.Lerp(cam.transform.localRotation, Quaternion.Euler(MP[2].Rotation), Time.deltaTime * 0.8f);
        }
        if(Back_B)
        {
            cam.transform.localPosition = Vector3.Lerp(cam.transform.localPosition, MP[0].Pos, Time.deltaTime * 0.8f);
            cam.transform.localRotation = Quaternion.Lerp(cam.transform.localRotation, Quaternion.Euler(MP[0].Rotation), Time.deltaTime * 0.8f);
        }
    }
    //0->1
    public void palate()
    {
        Palate_B = true;
        Wheel_B = false;
        Back_B = false;
    }
    //0->2
    public void wheel()
    {
        Palate_B = false;
        Wheel_B = true;
        Back_B = false;
    }
    //now ->0
    public void back()
    {
        Palate_B = false;
        Wheel_B = false;
        Back_B = true;        
    }
    
    public void exit()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<VehicleControl>().repair_on = false;
        GameObject.Find("CameraGroup").transform.GetChild(0).gameObject.SetActive(true);
        //GameObject.FindGameObjectWithTag("MainCamera").SetActive(true);
        GameObject.FindGameObjectWithTag("MainUI").transform.GetChild(0).gameObject.SetActive(true);
        GameObject.Find("RepairShop_Main").transform.GetChild(0).GetChild(0).gameObject.SetActive(false); //cam
        GameObject.Find("RepairShop_Main").transform.GetChild(4).gameObject.SetActive(false); //UI

        GameObject.Find("BGM").GetComponent<AudioSource>().enabled = true;
        GameObject.Find("BGM").GetComponent<AudioSource>().Play();
    }

}

