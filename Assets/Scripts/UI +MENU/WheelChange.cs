﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelChange : MonoBehaviour
{
    GameObject[] Wheels_sky;

    private void Start()
    {
        Wheels_sky = GameObject.FindGameObjectsWithTag("Wheelset");
    }
    public void sky()
    {
        for (int i = 0; i < Wheels_sky.Length; i++) {
            Wheels_sky[i].transform.GetChild(0).gameObject.SetActive(true);//sky    
                }
        for (int i = 0; i < Wheels_sky.Length; i++)
        {
            Wheels_sky[i].transform.GetChild(1).gameObject.SetActive(false); //evd
        }
    }
    public void evp()
    {
        for (int i = 0; i < Wheels_sky.Length; i++)
        {
            Wheels_sky[i].transform.GetChild(0).gameObject.SetActive(false);
        }
        for (int i = 0; i < Wheels_sky.Length; i++)
        {
            Wheels_sky[i].transform.GetChild(1).gameObject.SetActive(true);
        }
    }
}
