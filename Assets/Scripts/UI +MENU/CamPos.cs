﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamPos : MonoBehaviour
{
    public GameObject Player;
    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");

    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = Player.transform.position;
        this.transform.Translate (new Vector3(0, 2.44f, -8.98f));
        this.transform.rotation = Player.transform.rotation;
        this.transform.Rotate(new Vector3(-1.2f, 0, 0));
    }
}
