﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Select_camMove : MonoBehaviour
{
    Transform pos2d;
    Vector3 start = new Vector3(-463.9065f, -86.66125f, 358.8f);
    Vector3 end = new Vector3(-463.9065f, -86.66125f, -397.2f);
    private bool left= false;
    private bool right = false;
    public float speed = 100;
    public int Car_Num = 1;
    void Start()
    {
        pos2d = Camera.main.gameObject.transform;
    }
    void Update()
    {
        if (left) {
            pos2d.position = Vector3.MoveTowards(pos2d.position, start, speed * Time.deltaTime);

            Car_Num = 1;
        }
        if (right) {
            pos2d.position = Vector3.MoveTowards(pos2d.position, end, speed * Time.deltaTime);
            Car_Num = 2;
        }

        if (pos2d.position == start) {
            left = false; }
        if (pos2d.position == end) {
            right = false; }
    }
    
    public void Goleft()
    {

        GameObject.Find("Level").transform.GetChild(0).gameObject.SetActive(true);
        GameObject.Find("Level").transform.GetChild(1).gameObject.SetActive(false);
        left = true; 
    }
    public void GoRight()
    {

        GameObject.Find("Level").transform.GetChild(0).gameObject.SetActive(false);
        GameObject.Find("Level").transform.GetChild(1).gameObject.SetActive(true);
        right = true;
    }
}
