﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinnerspin : MonoBehaviour
{
    private Transform Spin2d;
    public float speed = 0.1f;
    //(-1.15f,0.86f,0.8f);
    // Start is called before the first frame update
    void Start()
    {
        Spin2d = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        Spin2d.Rotate(0, speed * Time.deltaTime, 0);
    }
}
