﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class iconVision : MonoBehaviour
{
    GameObject[] Question;
    GameObject[] Repair;
    GameObject Parent;
    Vector3 temp;
    public GameObject qus, rep;
    // Start is called before the first frame update
    void Start()
    {
        Parent= GameObject.Find("MapVision");

        for (int i = 0; i < GameObject.Find("QuestionGroup").transform.childCount; i++)
        {
            temp= GameObject.Find("QuestionGroup").transform.GetChild(i).gameObject.transform.position;
            temp.y = 2010;
            Instantiate(qus, temp, Quaternion.identity).transform.SetParent(Parent.transform);
        }
        for (int i = 0; i < GameObject.Find("RepairGroup").transform.childCount; i++)
        {
            temp = GameObject.Find("RepairGroup").transform.GetChild(i).gameObject.transform.position;
            temp.y = 2010;
            Instantiate(rep, temp, Quaternion.identity).transform.SetParent(Parent.transform);
        }

       
    }
}
