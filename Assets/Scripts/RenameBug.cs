﻿using UnityEngine;
using UnityEngine.UI;

public class ContentFitterFix : MonoBehaviour
{
    public ContentSizeFitter.FitMode horizontalFit;
    public ContentSizeFitter.FitMode verticalFit;

    ContentSizeFitter contentRef;

    void Awake()
    {
        contentRef = GetComponent<ContentSizeFitter>();
        contentRef.horizontalFit = horizontalFit;
        contentRef.verticalFit = verticalFit;
        Destroy(this);
    }
}