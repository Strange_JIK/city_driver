﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameInput : MonoBehaviour
{
    public GameObject PauseUI;
    public GameObject[] Hide_UI;

    private bool paused = false;

    void Start()
    {
        PauseUI.SetActive(false);
        Hide_UI = GameObject.FindGameObjectsWithTag("UI");
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            paused = !paused;
        }
        if (paused)
        {
            PauseUI.SetActive(true);
            for(int i=0; i<Hide_UI.Length; i++) { Hide_UI[i].SetActive(false);}

            Time.timeScale = 0;
        }

        if (!paused)
        {
            PauseUI.SetActive(false);
            for (int i = 0; i < Hide_UI.Length; i++) { Hide_UI[i].SetActive(true); }
            Time.timeScale = 1f;
        }
        //bool esc_D = Input.GetButtonDown("Cancel");
        //bool esc_U = Input.GetButtonUp("Cancel");
        //int count = 0;
        //if(esc_D && count == 0)
        //{
        //    Time.timeScale = 0.0f;
        //}
        //else if(esc_D && count == 1)
        //{
        //    Time.timeScale = 1.0f;
        //}
        //if (esc_U)
        //{
        //    count += 1;
        //    if (count > 1) count = 0;
        //}

    }
    public void resume()
    {
        paused = !paused;
    }
}
