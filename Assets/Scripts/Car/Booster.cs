﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Booster : MonoBehaviour
{
    
    public Transform Cam_T;
    public GameObject Booster_Particle;
    private float move,count;

    public void Start()
    {
        Cam_T = this.GetComponent<Transform>();
    }

    public void Set_Booster_act(bool on)
    {
        if (on)
        {
            Cam_T.GetComponent<Kino.Motion>().enabled = true;
        }
        else
        {
            Cam_T.GetComponent<Kino.Motion>().enabled = false;
        }
    }
   
   
}
