﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Booster_Geage : MonoBehaviour
{
    //기본 0
    //일정행동을 하면 부스터 게이지가 찬다
    //max 0.9 / 0.6 / 0.3 
    //부스터를 발동하면 게이지가 줄어든다/
    public float count = 0;
    public int Booster_C = 0;
    public float Fill_num=0;
    RectTransform RT;
    Image Img;
    Vector2 temp_W,temp_P;
    private void Start()
    {
        RT = this.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>();
        Img = this.transform.GetChild(0).GetChild(1).GetComponent<Image>();
    }
    public void Booster_Counter()
    {
        
        count += 0.1f;
        if(count < 0.3f) { Booster_C = 0; }
        else if(count >=0.3 && count < 0.6f) { Booster_C = 1; }
        else if (count >= 0.6 && count < 0.9f) { Booster_C = 2; }
        else if (count >= 0.9) { Booster_C = 3; }
        Debug.Log(Booster_C);
    }
    private void Update()
    {
        Stretch();
        Fill_num = Img.fillAmount;
    }
    void Stretch()
    {
        if (Img.fillAmount <= count)
        {
            Img.fillAmount += 0.004f;
        }
        if(Img.fillAmount > count)
        {
            Img.fillAmount -= 0.004f;
        }
    }
}
//게이지 찰때 부스터 발동 가능
//부스터 발동시 게이지 줄어듬
//단계별로 속도 ?