using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Vehicles.Car;


[RequireComponent(typeof(CarController))]
public class CarUserControl : MonoBehaviour
{
    private CarController m_Car; 
    private  GameObject Marker; // 마커. 

    
    private void Awake()
    {
       
        // get the car controller
        m_Car = GetComponent<CarController>();
        Marker = GameObject.Find("Marker");
    }


    private void FixedUpdate()
    {

        // pass the input to the car!
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
#if !MOBILE_INPUT
        // float booster = Input.GetAxis("Jump"); //space
        m_Car.Booster_B = Input.GetButtonDown("Jump");
        float handbrake=0;
        if (Input.GetButtonDown("Fire3")) handbrake = 1;
        else { handbrake = 0; }


        m_Car.Move(h, v, v, handbrake);
#else
            m_Car.Move(h, v, v, 0f);
#endif

        //지도 마커 표시
        //Marker.transform.position = new Vector3(this.transform.position.x, Marker.transform.position.y, this.transform.position.z);
        //Marker.transform.forward = this.transform.forward;

    }
}

